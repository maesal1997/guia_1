SELECT 
DISTINCT REMEIUIU.IPID_ID_INT,
CASE WHEN REMESAS.REME_RAZON_SOCIAL_REMTIE IS NULL  THEN REMESAS.REME_PRIMER_NOMBRE_REMITE ||' '||REMESAS.REME_SEGUNDO_NOMBRE_REMITE ||' '||REMESAS.REME_PRIMER_APELLIDO_REMITE ||' '||REMESAS.REME_SEGUNDO_APELLIDO_REMITE ELSE REMESAS.REME_RAZON_SOCIAL_REMTIE END AS NOMBRECOMPLETOREMITENTE, 
REMESAS.REME_NRO_IDENTIFICACION_REMITE AS NITREMITE,
CIUD_ORI.DESCRIPCION ||' ('|| CIUD_ORI.DEPA_DESCRIPCION ||' - '||CIUD_ORI.PAIS_ABREVIATURA||')' AS CIUD_ORIGEN,
REMESAS.REME_DIRECCION_REMITE, 
CASE WHEN LENGTH(REMESAS.REME_PRIMER_NOMBRE_DESTINO)>1 THEN REMESAS.REME_PRIMER_NOMBRE_DESTINO ||' '||REMESAS.REME_SEGUNDO_NOMBRE_DESTINO ||' '||REMESAS.REME_PRIMER_APELLIDO_DESTINO ||' '||REMESAS.REME_SEGUNDO_APELLIDO_DESTINO ELSE REMESAS.REME_RAZON_SOCIAL_DESTINO END AS NOMBRECOMPLETODESTINO,
REMESAS.REME_PRIMER_NOMBRE_DESTINO AS NITDESTINATARIO,
CIUD_DES.DESCRIPCION ||' ('|| CIUD_DES.DEPA_DESCRIPCION ||' - '||CIUD_DES.PAIS_ABREVIATURA||')' AS CIUD_DESTINO,
REMESAS.REME_DIRECCION_DESTINO,

UNNE.UNNE_DESCRIPCION,
MAES.ESTA_DESCRIPCION,

REMEIUIU.IPID_CODIGO_BARRAS,
MATIUN.TIUN_DESCRIPCION,
REMEIUIU.IPID_KILOS_REALES,
REMEIUIU.IPID_LARGO,
REMEIUIU.IPID_ALTO,
REMEIUIU.IPID_ANCHO,
'' AS NUEVO_CODIGO


FROM REM_REMESAS REMESAS
INNER JOIN MAE_UNIDAD_NEGOCIOS UNNE ON REMESAS.UNNE_ID_INT = UNNE.UNNE_ID_INT
INNER JOIN MAE_ESTADO_OBJETO_NEGOCIO MEON ON REMESAS.ESON_ID_INT = MEON.ESON_ID_INT
INNER JOIN MAE_ESTADOS MAES ON MEON.ESTA_ID_INT = MAES.ESTA_ID_INT
INNER JOIN MAE_RUTAS RUTA ON REMESAS.RUTA_ID_INT  = RUTA.RUTA_ID_INT
INNER JOIN MAE_CIUDAD_SITIOS_COMUNES2_VM CIUD_ORI ON CIUD_ORI.ID = RUTA.RUTA_ID_GEOGRAFIA_ORIGEN AND RUTA.TIGE_ID_INT_ORIGEN  = CIUD_ORI.TIPO_GEO 
INNER JOIN MAE_CIUDAD_SITIOS_COMUNES2_VM CIUD_DES ON CIUD_DES.ID = RUTA.RUTA_ID_GEOGRAFIA_DESTINO AND RUTA.TIGE_ID_INT_DESTINO  = CIUD_DES.TIPO_GEO
INNER JOIN REM_REMESA_EXT REMEEX ON REMESAS.REME_ID_INT = REMEEX.REME_ID_INT  
INNER JOIN REM_IUP_IUD REMEIUIU ON REMESAS.REME_ID_INT = REMEIUIU.REME_ID_INT
INNER JOIN MAE_TIPO_UNIDADES MATIUN ON REMEIUIU.TIUN_ID_INT = MATIUN.TIUN_ID_INT
LEFT  JOIN (SELECT REMEIUIUTRA.IPID_ID_INT, COUNT(*) CANTIDRL FROM REM_IUP_IUD_TRAZABILIDAD REMEIUIUTRA WHERE  REMEIUIUTRA.PROC_ID_INT = 8 group by REMEIUIUTRA.IPID_ID_INT) DRL ON REMEIUIU.IPID_ID_INT = DRL.IPID_ID_INT
WHERE  (REMESAS.REME_NUMERO_REMESA = :NUMERO_REMESA)
AND ((REMESAS.ESON_ID_INT = 196) OR 
(REMESAS.ESON_ID_INT = 205 AND REMEEX.SIIN_ID_INT = 52 AND DRL.CANTIDRL < 1))



