package co.com.tcc.maestro.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

public class ItemLista {

	@JsonSerialize(using = ToStringSerializer.class)
	private BigDecimal id;

	private String descripcion;

	private String abreviatura;

	/**
	 * El atributo valor es el id verdadero
	 */
	private String valor;

	public ItemLista() {

	}

	public ItemLista(BigDecimal id, String descripcion, String abreviatura, String valor) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.abreviatura = abreviatura;
		this.valor = valor;
	}

	public ItemLista(String valor) {

		this.valor = valor;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

	@Override
	public String toString() {
		return "ItemLista [id=" + id + ", descripcion=" + descripcion + ", abreviatura=" + abreviatura + "]";
	}

}
