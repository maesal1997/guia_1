package co.com.tcc.maestro.web.api;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.amazonaws.services.cloudfront.model.AccessDeniedException;

import co.com.tcc.maestro.dto.Mensaje;
import co.com.tcc.maestro.dto.ObjMensajes;
import co.com.tcc.maestro.exception.AccesoDatosException;
import co.com.tcc.maestro.exception.ObjetoNoEncontradoException;
import co.com.tcc.maestro.exception.ValidacionDominioException;

@RestControllerAdvice
public class GlobalControllerExceptionHandler {

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	ObjMensajes mensaje = new ObjMensajes();
	@ExceptionHandler({ ObjetoNoEncontradoException.class,
			AccesoDatosException.class })
	public Mensaje handleExceptions(Exception e, HttpServletResponse response) {
		if (e instanceof ObjetoNoEncontradoException)
			response.setStatus(404);
		else
			response.setStatus(400);

		return new Mensaje(e.getMessage());
	}

	@ExceptionHandler({ AccessDeniedException.class })
	public Mensaje handleAccesDeniedException(Exception e, HttpServletResponse response) {
		response.setStatus(403);

		return new Mensaje("Acceso denegado");
	}

	@ExceptionHandler({ PermissionDeniedDataAccessException.class })
	public Mensaje handleDataAccesException(Exception e, HttpServletResponse response) {
		response.setStatus(403);
		e.printStackTrace();
		return new Mensaje("No tiene permisos en la base de datos para completar esta acci�n");
	}

	@ExceptionHandler({ SQLException.class })
	public ObjMensajes handleSQLException(Exception e, HttpServletResponse response) {
		response.setStatus(500);
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		e.printStackTrace();
		return new ObjMensajes(-1, sw.toString(), -1, "Ocurrio un error inesperado durante el proceso:");
	}

	@ExceptionHandler({ Exception.class })
	public ObjMensajes handleNullExceptions(Exception e, HttpServletResponse response) {
		response.setStatus(500);
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		e.printStackTrace();
		return new ObjMensajes(-1, sw.toString(), -1, "Ocurrio un error inesperado durante el proceso:");
	}

	@ExceptionHandler({ RuntimeException.class })
	public ObjMensajes runtimeException(RuntimeException e, HttpServletResponse response) {
		response.setStatus(500);
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		e.printStackTrace();
		
		return new ObjMensajes(-1, sw.toString(), -1, "Ocurrio un error inesperado durante el proceso:");
	}
	
	@ExceptionHandler({ ValidacionDominioException.class })
	public ObjMensajes manejarValidacionDominioException(RuntimeException e, HttpServletResponse response) {
		response.setStatus(400);
		ValidacionDominioException validacionDominioException = (ValidacionDominioException) e;
		return new ObjMensajes(-1, validacionDominioException.getMensaje().getMensajeSistema(), -1,  validacionDominioException.getMensaje().getMensajeUsuario());
	}

}