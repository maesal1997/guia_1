package co.com.tcc.maestro.dao;

import java.util.List;

public interface TokenDao {

	public List<String> validarToken(String token);
}
