package co.com.tcc.maestro.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

public class DatosTrazaUnidad {
	@JsonSerialize(using = ToStringSerializer.class)
	private BigDecimal idUnidad;
	private String ubicacionTcc;
	public BigDecimal getIdUnidad() {
		return idUnidad;
	}
	public void setIdUnidad(BigDecimal idUnidad) {
		this.idUnidad = idUnidad;
	}
	public String getUbicacionTcc() {
		return ubicacionTcc;
	}
	public void setUbicacionTcc(String ubicacionTcc) {
		this.ubicacionTcc = ubicacionTcc;
	}
	@Override
	public String toString() {
		return "DatosTrazaUnidad [idUnidad=" + idUnidad + ", ubicacionTcc=" + ubicacionTcc + "]";
	}
	
}
