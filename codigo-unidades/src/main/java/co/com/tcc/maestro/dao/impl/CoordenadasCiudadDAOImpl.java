package co.com.tcc.maestro.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dao.ICoordenadasCiudadDAO;
import co.com.tcc.maestro.dao.rowmapper.CoordenadasRowMapper;
import co.com.tcc.maestro.dto.Coordenada;

@Repository
public class CoordenadasCiudadDAOImpl implements ICoordenadasCiudadDAO {

	@Autowired
	@Qualifier("jdbcTemplateConsultaGeneral")
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Coordenada> obtenerCoodenadasCiudad(String ciudad) {

		List<Coordenada> coodenadas = new ArrayList<Coordenada>();
		String sql = "select dire.DIRE_LATITUD, dire.DIRE_LONGITUD from MAE_CIUDAD_SITIOS_COMUNES3_VM vm              \r\n" + 
				"inner join MAE_PUNTO_VENTA_V pls on vm.LOCA_ID_INT = pls.LOCA_ID_INT_CO and vm.id = pls.CIUD_ID_INT  \r\n" + 
				"inner Join MAE_DIRECCIONES dire on pls.DIRE_ID_INT = dire.DIRE_ID_INT                                \r\n" + 
				"where vm.id = " + ciudad;
		
		coodenadas = jdbcTemplate.query(sql, new CoordenadasRowMapper());
		
		return coodenadas;
	}
	



}
