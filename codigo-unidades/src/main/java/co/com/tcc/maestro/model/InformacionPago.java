package co.com.tcc.maestro.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

public class InformacionPago {

	@JsonSerialize(using = ToStringSerializer.class)
	private BigDecimal id;

	private EntidadFinanciera banco;

	private ItemLista tipoCuenta;

	private String numeroCuenta;

	private boolean cuentaNoPropia;

	private String nombreTitular;

	private String identificacionTitular;

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public EntidadFinanciera getBanco() {
		return banco;
	}

	public void setBanco(EntidadFinanciera banco) {
		this.banco = banco;
	}

	public ItemLista getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(ItemLista tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public boolean isCuentaNoPropia() {
		return cuentaNoPropia;
	}

	public void setCuentaNoPropia(boolean cuentaNoPropia) {
		this.cuentaNoPropia = cuentaNoPropia;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public String getIdentificacionTitular() {
		return identificacionTitular;
	}

	public void setIdentificacionTitular(String identificacionTitular) {
		this.identificacionTitular = identificacionTitular;
	}

}
