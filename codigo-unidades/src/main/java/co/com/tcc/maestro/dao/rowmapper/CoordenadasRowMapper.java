package co.com.tcc.maestro.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.com.tcc.maestro.dto.Coordenada;

public class CoordenadasRowMapper implements RowMapper<Coordenada> {

	@Override
	public Coordenada mapRow(ResultSet rs, int rowNum) throws SQLException {
		Coordenada coordenas = new Coordenada();
		coordenas.setLatitud(rs.getString("DIRE_LATITUD"));
		coordenas.setLongitud(rs.getString("DIRE_LONGITUD"));
		return coordenas;
	}

}
