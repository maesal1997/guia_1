package co.com.tcc.maestro.dao.impl;

import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;


import co.com.tcc.maestro.dao.IConsultaGeneralDao;
import co.com.tcc.maestro.dao.rowmapper.ConsultaGeneralRowMapper;
import co.com.tcc.maestro.dto.Respuesta;
import co.com.tcc.maestro.dto.ResultadoConsultaGeneral;

@Repository
public class ConsultaGeneralDaoImpl implements IConsultaGeneralDao {

	@Autowired
	@Qualifier("jdbcTemplateConsultaGeneral")
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Value("classpath:dao/consultaGeneral/consultaGeneral.txt")
	private Resource consultaGeneralCodigoUnidades;

	@Override
	public Respuesta consultaConexionDmPlataforma(String numeroGuia) throws IOException {
		
		
		Respuesta respuesta = new Respuesta();
		respuesta.setStatus(200);
		respuesta.setCodigoUsuario(0);
		respuesta.setMessage("La consulta se ha realizado con �xito");
		
		int aplicaFiltroNumeroRemesa = numeroGuia == null ? 0 : 1;
		
		MapSqlParameterSource parametros = new MapSqlParameterSource();

		parametros.addValue("APLICA_NUMERO_REMESA", aplicaFiltroNumeroRemesa);
		parametros.addValue("NUMERO_REMESA", numeroGuia == null ? "" : numeroGuia);
		String sql = IOUtils.toString(consultaGeneralCodigoUnidades.getInputStream());
		List<ResultadoConsultaGeneral> consulta = jdbcTemplate.query(sql, parametros, new ConsultaGeneralRowMapper());
		
		respuesta.setData(consulta);
		return respuesta;
	}

	

}
