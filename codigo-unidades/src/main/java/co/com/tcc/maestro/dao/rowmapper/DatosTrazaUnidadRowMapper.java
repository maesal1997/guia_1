package co.com.tcc.maestro.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.com.tcc.maestro.dto.DatosTrazaUnidad;

public class DatosTrazaUnidadRowMapper implements RowMapper<DatosTrazaUnidad> {

	@Override
	public DatosTrazaUnidad mapRow(ResultSet rs, int rowNum) throws SQLException {
		DatosTrazaUnidad datosTrazaUnidad = new DatosTrazaUnidad();
		datosTrazaUnidad.setIdUnidad(rs.getBigDecimal("IPID_ID_INT"));
		datosTrazaUnidad.setUbicacionTcc(rs.getString("UBTCC_ID_INT"));
		return datosTrazaUnidad;
	}

}
