package co.com.tcc.maestro.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dao.IActualizarCodigoUnidadDao;
import co.com.tcc.maestro.dto.ResultadoConsultaGeneral;
import co.com.tcc.maestro.utils.Parametros;

@Repository
public class ActualizarCodigoUnidadDaoImpl implements IActualizarCodigoUnidadDao{

	@Autowired
	@Qualifier("jdbcTemplateConsultaGeneral")
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private ParametrosDao parametrosDao; 
	
	@Override
	public void actualizarCodigoUnidad(final List<ResultadoConsultaGeneral> data, 
			                          String usuario,
			                          String ciudad,
			                          String longitud,
			                          String latitud) {
		
		String sql = "UPDATE REM_IUP_IUD SET IPID_CODIGO_BARRAS = ? WHERE IPID_CODIGO_BARRAS = ?";
		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ResultadoConsultaGeneral resultadoConsultaGeneral = data.get(i);
			
				ps.setString(1, resultadoConsultaGeneral.getNuevoCodigo());
				ps.setString(2, resultadoConsultaGeneral.getCodigoUnidad());
				
			}
			@Override
			public int getBatchSize() {
				return data.size();
			}
		});
	
		insertarTrazabilidadUnidades(data, usuario, ciudad, longitud, latitud);
	}
		
	protected BigDecimal consultarSecuenciaRemIupIudTrazabilidad() {
		String sql = "SELECT traer_secuencia('REM_IUP_IUD_TRAZABILIDAD') AS SECUENCIA FROM DUAL";
		return jdbcTemplate.queryForObject(sql, BigDecimal.class);
	}

	
	protected void insertarTrazabilidadUnidades(final List<ResultadoConsultaGeneral> data, 
											    String usuario,
									            String ciudad,
									            String longitud,
									            String latitud) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(cal.getTime());
		final Timestamp fecha = new Timestamp(cal.getTimeInMillis());
	    final int ciudIdInt = Integer.parseInt(ciudad);
	    final double longitu = Double.parseDouble(longitud);
	    final double latitu = Double.parseDouble(latitud);
	    final String user = usuario;
	    final int estadoOperativo = Integer.parseInt(parametrosDao.consultarParametro(Parametros.ESON_REMESA_IMPO)); 
	    final int proceso = Integer.parseInt(parametrosDao.consultarParametro(Parametros.PROCESO_RECOLECCION));
		String sql = "INSERT INTO REM_IUP_IUD_TRAZABILIDAD ("
				+ "IUTR_ID_INT,        \r\n" + 
				"IPID_ID_INT,          \r\n" + 
				"CIUD_ID_INT,          \r\n" + 
				"IUTR_LATITUD,         \r\n" + 
				"IUTR_LONGITUD,        \r\n" + 
				"UBTCC_ID_INT,         \r\n" + 
				"PROC_ID_INT,          \r\n" + 
				"IUTR_VISIBILIDAD,     \r\n" + 
				"IUTR_ESTADO,          \r\n" + 
				"IUTR_ACTIVACION,      \r\n" + 
				"IUTR_INACTIVACION,    \r\n" + 
				"IUTR_MODIFICACION,    \r\n" + 
				"IUTR_USUARIO,         \r\n" + 
				"IUTR_ID_REGISTRO,     \r\n" + 
				"IUTR_FEPROCESO,       \r\n" + 
				"ESON_ID_INT,          \r\n" + 
				"IUTR_OBSERVACIONES,   \r\n" + 
				"DIMO_ID_INT ) ";
		
		sql += " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ResultadoConsultaGeneral resultadoConsultaGeneral = data.get(i);
			
				ps.setBigDecimal(1, consultarSecuenciaRemIupIudTrazabilidad());
				ps.setBigDecimal(2, resultadoConsultaGeneral.getIdUnidad());
				ps.setInt(3, ciudIdInt);
				ps.setDouble(4, latitu);
				ps.setDouble(5, longitu);
				ps.setInt(6,  Integer.parseInt(resultadoConsultaGeneral.getUbicacionTcc()));
				ps.setInt(7, proceso);
				ps.setString(8, "1");
				ps.setString(9, "A");
				ps.setTimestamp(10, fecha);
				ps.setNull(11, java.sql.Types.TIMESTAMP);
				ps.setTimestamp(12, fecha);
				ps.setString(13, user);
				ps.setInt(14, 0);
				ps.setTimestamp(15, fecha);
				ps.setInt(16, estadoOperativo);
				ps.setString(17, "Actualizaci�n de c�digo de barras, "+resultadoConsultaGeneral.getCodigoUnidad()+" Reemplazado por "+resultadoConsultaGeneral.getNuevoCodigo());
				ps.setNull(18, java.sql.Types.INTEGER);
			}
			@Override
			public int getBatchSize() {
				return data.size();
			}
		});
	}
}
