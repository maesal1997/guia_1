package co.com.tcc.maestro.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.ICoordenadasCiudadDAO;
import co.com.tcc.maestro.dto.Coordenada;
import co.com.tcc.maestro.dto.ObjMensajes;
import co.com.tcc.maestro.exception.ValidacionDominioException;
import co.com.tcc.maestro.service.ICoordenadasCiudadService;

@Service
public class CoordenadasCiudadServiceImpl implements ICoordenadasCiudadService {

	@Autowired
	ICoordenadasCiudadDAO coordenadasCiudadDAO;
	
	@Override
	public List<Coordenada> obtenerCoodenadasCiudad(String ciudad) {
		
		List<Coordenada> coodenadas = new ArrayList<Coordenada>();
		coodenadas = coordenadasCiudadDAO.obtenerCoodenadasCiudad(ciudad);
		if (coodenadas.isEmpty()) {
			ObjMensajes objMensajes = new ObjMensajes();
			objMensajes.setMensajeSistema("No se han encontrado longitud y latitud para la ciudad");
		    objMensajes.setMensajeUsuario("No se han encontrado longitud y latitud para la ciudad");		
			throw new ValidacionDominioException(objMensajes);
		}
		return coodenadas;
	}

}
