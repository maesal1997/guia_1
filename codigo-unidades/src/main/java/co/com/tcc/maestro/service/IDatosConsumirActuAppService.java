package co.com.tcc.maestro.service;

import java.util.List;

import co.com.tcc.maestro.dto.DatosUser;

public interface IDatosConsumirActuAppService {
	public List<DatosUser> obtenerUsuario();
	public List<DatosUser> obtenerCiudadUsuarioToken ( String token );

}
