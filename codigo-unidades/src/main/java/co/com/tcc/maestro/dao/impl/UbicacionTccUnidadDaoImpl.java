package co.com.tcc.maestro.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dao.IUbicacionTccUnidadDao;
import co.com.tcc.maestro.dao.rowmapper.DatosTrazaUnidadRowMapper;
import co.com.tcc.maestro.dto.DatosTrazaUnidad;

@Repository
public class UbicacionTccUnidadDaoImpl implements IUbicacionTccUnidadDao{

	@Autowired
	@Qualifier("jdbcTemplateConsultaGeneral")
	JdbcTemplate jdbcTemplate;
	
	@Override
	public List<DatosTrazaUnidad> obtenerUbicacion(String codigosUnidades) {

		String sql = " SELECT IPID_ID_INT, UBTCC_ID_INT FROM REM_IUP_IUD WHERE IPID_CODIGO_BARRAS IN ("+codigosUnidades+")";
		return jdbcTemplate.query(sql, new DatosTrazaUnidadRowMapper() );
	}

}
