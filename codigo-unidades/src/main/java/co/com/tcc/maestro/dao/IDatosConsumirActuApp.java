package co.com.tcc.maestro.dao;

import java.util.List;

import co.com.tcc.maestro.dto.DatosUser;

public interface IDatosConsumirActuApp {
	
	public List<DatosUser> obtenerUsuario();
	public List<DatosUser> obtenerCiudadUsuarioToken ( String token );

}
