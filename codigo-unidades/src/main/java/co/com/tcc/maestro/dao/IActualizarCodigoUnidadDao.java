package co.com.tcc.maestro.dao;

import java.util.List;

import co.com.tcc.maestro.dto.ResultadoConsultaGeneral;

public interface IActualizarCodigoUnidadDao {

	public void actualizarCodigoUnidad(final List<ResultadoConsultaGeneral> data,
										String usuario,
										String ciudad,
										String longitud,
										String latidud);
}
