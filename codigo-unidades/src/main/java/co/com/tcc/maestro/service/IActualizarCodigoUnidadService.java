package co.com.tcc.maestro.service;

import java.util.List;

import co.com.tcc.maestro.dto.ResultadoConsultaGeneral;

public interface IActualizarCodigoUnidadService {
	public void actualizarCodigoUnidad(final List<ResultadoConsultaGeneral> data,
			                           String token,
			                           String usuario,
			                           String ciudad,
			                           String longitud,
			                           String latidud);

}
