package co.com.tcc.maestro.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dao.IValidarCodigoUnidadDao;

@Repository
public class ValidarCodigoUnidadDao implements IValidarCodigoUnidadDao {
  
	@Autowired
	@Qualifier("jdbcTemplateConsultaGeneral")
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<String> validarCodigoUnidad(String nuevoCodigoUnidad) {


		String sql = " SELECT IPID_CODIGO_BARRAS FROM REM_IUP_IUD WHERE IPID_CODIGO_BARRAS IN ("+nuevoCodigoUnidad+")";
		return jdbcTemplate.queryForList(sql, String.class);
		
	}

	
	
}
