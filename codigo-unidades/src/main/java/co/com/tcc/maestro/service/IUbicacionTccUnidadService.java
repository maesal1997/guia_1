package co.com.tcc.maestro.service;

import java.util.List;

import co.com.tcc.maestro.dto.DatosTrazaUnidad;

public interface IUbicacionTccUnidadService {
	
	public List<DatosTrazaUnidad> obtenerUbicacion(String codigosUnidades);

}
