package co.com.tcc.maestro.utils;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class CustomTimeSerializer extends StdSerializer<Timestamp> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomTimeSerializer() {
		this(null);
	}

	public CustomTimeSerializer(Class<Timestamp> t) {
		super(t);
	}

	@Override
	public void serialize(Timestamp value, JsonGenerator jgen, SerializerProvider provider)
			throws IOException, JsonProcessingException {

		DateFormat df = new SimpleDateFormat("hh:mm a");

		jgen.writeString(df.format(value));

	}

}