package co.com.tcc.maestro.utils;

import java.util.concurrent.TimeUnit;

import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;


public class CacheReporte {
	
	private static CacheReporte instance;
	
	private Cache<String,String> cache;
	
	public Cache<String, String> getCache() {
		return cache;
	}

	private CacheReporte(){
		cache = new Cache2kBuilder<String, String>() {}
	    .expireAfterWrite(30, TimeUnit.MINUTES)    // expire/refresh after 5 minutes
	    .build();
	}
	
	public static CacheReporte getInstance(){
		if(instance == null)
			instance = new CacheReporte();
		return instance;
	}
	
	

}
