package co.com.tcc.maestro.model;

public class Persona extends Tercero {

	private String primerNombre;

	private String segundoNombre;

	private String primerApellido;

	private String segundoApellido;

	private String nombre;

	private Ciudad ciudadResidencia;

	public String getNombreCompleto() {
		return primerNombre + " " + segundoNombre + " " + primerApellido + " " + segundoApellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public Ciudad getCiudadResidencia() {
		return ciudadResidencia;
	}

	public void setCiudadResidencia(Ciudad ciudadResidencia) {
		this.ciudadResidencia = ciudadResidencia;
	}

}
