package co.com.tcc.maestro.service;

import java.io.IOException;

import co.com.tcc.maestro.dto.Respuesta;

public interface IConsultaGeneralService {
	public Respuesta consultaConexionDmPlataforma(String numeroGuia) throws IOException;
}
