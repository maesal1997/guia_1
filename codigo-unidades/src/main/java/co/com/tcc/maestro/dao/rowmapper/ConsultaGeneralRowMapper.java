package co.com.tcc.maestro.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.com.tcc.maestro.dto.ResultadoConsultaGeneral;

public class ConsultaGeneralRowMapper implements RowMapper<ResultadoConsultaGeneral> {

	@Override
	public ResultadoConsultaGeneral mapRow(ResultSet rs, int rowNum) throws SQLException {
		ResultadoConsultaGeneral resultadoConsultaGeneral = new ResultadoConsultaGeneral();
		
		resultadoConsultaGeneral.setNombreRemite(rs.getString("NOMBRECOMPLETOREMITENTE"));
		resultadoConsultaGeneral.setNitRemite(rs.getString("NITREMITE"));
		resultadoConsultaGeneral.setCiudadRemite(rs.getString("CIUD_ORIGEN"));
		resultadoConsultaGeneral.setDireccionRemite(rs.getString("REME_DIRECCION_REMITE"));
		resultadoConsultaGeneral.setNombreDestinatario(rs.getString("NOMBRECOMPLETODESTINO"));
		resultadoConsultaGeneral.setNitDestinatario(rs.getString("NITDESTINATARIO"));
		resultadoConsultaGeneral.setCiudadDestinatario(rs.getString("CIUD_DESTINO"));
		resultadoConsultaGeneral.setDireccionDestinatario(rs.getString("REME_DIRECCION_DESTINO"));
		resultadoConsultaGeneral.setUnidadNegocio(rs.getString("UNNE_DESCRIPCION"));
		resultadoConsultaGeneral.setEstadoOperativo(rs.getString("ESTA_DESCRIPCION"));
		resultadoConsultaGeneral.setCodigoUnidad(rs.getString("IPID_CODIGO_BARRAS"));
		resultadoConsultaGeneral.setTipoUnidad(rs.getString("TIUN_DESCRIPCION"));
		resultadoConsultaGeneral.setAlto(rs.getString("IPID_ALTO"));
		resultadoConsultaGeneral.setAncho(rs.getString("IPID_ANCHO"));
		resultadoConsultaGeneral.setLargo(rs.getString("IPID_LARGO"));
		resultadoConsultaGeneral.setPesoReal(rs.getString("IPID_KILOS_REALES"));
		resultadoConsultaGeneral.setIdUnidad(rs.getBigDecimal("IPID_ID_INT"));
		resultadoConsultaGeneral.setNuevoCodigo(rs.getString("NUEVO_CODIGO"));
		
		
		
		return resultadoConsultaGeneral;
	}

}
