package co.com.tcc.maestro.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.IConsultaGeneralDao;
import co.com.tcc.maestro.dto.ObjMensajes;
import co.com.tcc.maestro.dto.Respuesta;
import co.com.tcc.maestro.exception.ValidacionDominioException;
import co.com.tcc.maestro.service.IConsultaGeneralService;

@Service
public class ConsultaGeneralServiceImpl implements IConsultaGeneralService {

	@Autowired
	private IConsultaGeneralDao servicioDao;

	@Override
	public Respuesta consultaConexionDmPlataforma(String numeroGuia) throws IOException {
        Respuesta respuesta = new Respuesta();
        respuesta = servicioDao.consultaConexionDmPlataforma(numeroGuia);
        if (respuesta.getData().isEmpty()) {
        	ObjMensajes objMensajes = new ObjMensajes();
			objMensajes.setMensajeSistema("El n�mero de gu�a no cumple con los criterios para actualizar sus unidades");
		    objMensajes.setMensajeUsuario("El n�mero de gu�a no cumple con los criterios para actualizar sus unidades");		
			throw new ValidacionDominioException(objMensajes);
        }
		return respuesta;
	}

}
