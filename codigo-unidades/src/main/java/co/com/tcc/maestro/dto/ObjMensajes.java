package co.com.tcc.maestro.dto;

public class ObjMensajes  {

	private int codigoSistema;
	private String mensajeSistema;
	private int codigoUsuario;
	private String mensajeUsuario;
	
	
	
	public ObjMensajes() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ObjMensajes(int codigoSistema, String mensajeSistema, int codigoUsuario, String mensajeUsuario) {
		this.codigoSistema = codigoSistema;
		this.mensajeSistema = mensajeSistema;
		this.codigoUsuario = codigoUsuario;
		this.mensajeUsuario = mensajeUsuario;
	}
	public String getMensajeSistema() {
		return mensajeSistema;
	}
	public void setMensajeSistema(String mensajeSistema) {
		this.mensajeSistema = mensajeSistema;
	}
	
	public String getMensajeUsuario() {
		return mensajeUsuario;
	}
	public void setMensajeUsuario(String mensajeUsuario) {
		this.mensajeUsuario = mensajeUsuario;
	}
	
	public int getCodigoSistema() {
		return codigoSistema;
	}
	public void setCodigoSistema(int codigoSistema) {
		this.codigoSistema = codigoSistema;
	}
	public int getCodigoUsuario() {
		return codigoUsuario;
	}
	public void setCodigoUsuario(int codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	@Override
	public String toString() {
		return "ObjMensajesType [codigoSistema=" + codigoSistema + ", mensajeSistema=" + mensajeSistema
				+ ", codigoUsuario=" + codigoUsuario + ", mensajeUsuario=" + mensajeUsuario + "]";
	}



}
