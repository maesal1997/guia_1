package co.com.tcc.maestro.dao;

import java.util.List;

import co.com.tcc.maestro.dto.Coordenada;

public interface ICoordenadasCiudadDAO {
	
	public List<Coordenada> obtenerCoodenadasCiudad(String ciudad);

}
