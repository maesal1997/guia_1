package co.com.tcc.maestro.dto.oracle;

import java.math.BigDecimal;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import oracle.sql.STRUCT;

public class ObjMensajesType implements SQLData {

	private BigDecimal codigoSistema;
	private String mensajeSistema;
	private BigDecimal codigoUsuario;
	private String mensajeUsuario;

	@Override
	public String getSQLTypeName() throws SQLException {
		return "OBJ_MENSAJES_T";
	}

	@Override
	public void readSQL(SQLInput stream, String typeName) throws SQLException {
		codigoSistema = stream.readBigDecimal();
		mensajeSistema = stream.readString();
		codigoUsuario = stream.readBigDecimal();
		mensajeUsuario = stream.readString();
	}

	public void setStruct(STRUCT structura) throws Exception {
		Object[] attributes = structura.getAttributes();

		if (attributes[0] != null) {
			this.setCodigoSistema(((java.math.BigDecimal) attributes[0]));
		}
		if (attributes[1] != null) {
			this.setMensajeSistema((String) attributes[1]);
		}

		if (attributes[2] != null) {
			this.setCodigoUsuario(((java.math.BigDecimal) attributes[2]));
		}
		if (attributes[3] != null) {
			this.setMensajeUsuario((String) attributes[3]);
		}

	}

	@Override
	public void writeSQL(SQLOutput stream) throws SQLException {
		stream.writeBigDecimal(codigoSistema);
		stream.writeString(mensajeSistema);
		stream.writeBigDecimal(codigoUsuario);
		stream.writeString(mensajeUsuario);
	}

	public BigDecimal getCodigoSistema() {
		return codigoSistema;
	}

	public void setCodigoSistema(BigDecimal codigoSistema) {
		this.codigoSistema = codigoSistema;
	}

	public String getMensajeSistema() {
		return mensajeSistema;
	}

	public void setMensajeSistema(String mensajeSistema) {
		this.mensajeSistema = mensajeSistema;
	}

	public BigDecimal getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(BigDecimal codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	public String getMensajeUsuario() {
		return mensajeUsuario;
	}

	public void setMensajeUsuario(String mensajeUsuario) {
		this.mensajeUsuario = mensajeUsuario;
	}

	@Override
	public String toString() {
		return "ObjMensajesType [codigoSistema=" + codigoSistema + ", mensajeSistema=" + mensajeSistema
				+ ", codigoUsuario=" + codigoUsuario + ", mensajeUsuario=" + mensajeUsuario + "]";
	}

}
