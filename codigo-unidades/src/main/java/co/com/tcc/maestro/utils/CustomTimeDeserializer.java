package co.com.tcc.maestro.utils;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import co.com.tcc.maestro.dto.ObjMensajes;
import co.com.tcc.maestro.exception.ValidacionDominioException;

public class CustomTimeDeserializer extends JsonDeserializer<Timestamp> {

	@Override
	public Timestamp deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fechaActual = df.format(new Date());
		String date = fechaActual + " " + p.getText();

		if (p.getText().isEmpty()) {
			return null;
		}

		return convertStringToTimestamp(date);
	}

	public Timestamp convertStringToTimestamp(String strDate) {
		try {
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
			// you can change format of date
			Date date = formatter.parse(strDate);

			return new Timestamp(date.getTime());
		} catch (ParseException e) {
			ObjMensajes objMensajes = new ObjMensajes();
			objMensajes.setMensajeSistema("Ocurri� un error durante el proceso: " + e);
			objMensajes.setMensajeUsuario("Ocurri� un error durante el proceso: " + e);	
			throw new ValidacionDominioException(objMensajes);

		}
	}
}