package co.com.tcc.maestro.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.com.tcc.maestro.dto.DatosUser;

public class DatosUserTokenRowMapper implements RowMapper<DatosUser>{

	@Override
	public DatosUser mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		DatosUser datosUserToken = new DatosUser();
		datosUserToken.setCiudad(rs.getString("CIUD_ID_INT"));
		datosUserToken.setUsuario(rs.getString("TOKE_USUARIO"));
		
		return datosUserToken;
	}

}
