/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.tcc.maestro.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

/**
 *
 * @author softcaribbean 54
 */
public class ReportUtil {

	public static byte[] exportToExcel(JasperReport reporte, Map<String, Object> parameters, Object data,
			HttpServletResponse response, String nombreReporte) throws JRException, IOException {

		parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		byte[] archivo = null;

		File temp = File.createTempFile("temp-file-name", ".tmp");

		JRXlsExporter exporter = new JRXlsExporter();

		JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parameters,
				new JRBeanCollectionDataSource((Collection<?>) data));

		exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(temp));
		SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
		configuration.setOnePagePerSheet(false);
		configuration.setIgnoreGraphics(false);
		configuration.setCollapseRowSpan(true);
		// configuration.setIgnoreCellBackground(Boolean.TRUE);
		configuration.setIgnoreCellBorder(true);
		configuration.setWhitePageBackground(false);
		configuration.setRemoveEmptySpaceBetweenRows(true);
		configuration.setCollapseRowSpan(true);

		exporter.setConfiguration(configuration);
		exporter.exportReport();

		archivo = readFileToByteArray(temp);
		temp.delete();
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-disposition", "attachment; filename=" + nombreReporte + ".xls");

		return archivo;
	}

	public static byte[] exportToPdf(JasperReport reporte, Map<String, Object> parameters, File file, List<Object> data)
			throws JRException, IOException {

		byte[] archivo;

		archivo = JasperRunManager.runReportToPdf(file.getPath(), parameters, new JRBeanCollectionDataSource(data));
		return archivo;
	}

	private static byte[] readFileToByteArray(File file) {
		FileInputStream fis = null;
		// Creating a byte array using the length of the file
		// file.length returns long which is cast to int
		byte[] bArray = new byte[(int) file.length()];
		try {
			fis = new FileInputStream(file);
			fis.read(bArray);
			fis.close();

		} catch (IOException ioExp) {
			ioExp.printStackTrace();
		}
		return bArray;
	}

}
