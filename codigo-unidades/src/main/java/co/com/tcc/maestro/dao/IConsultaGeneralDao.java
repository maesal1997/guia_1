package co.com.tcc.maestro.dao;

import java.io.IOException;

import co.com.tcc.maestro.dto.Respuesta;

public interface IConsultaGeneralDao{
  
	public Respuesta consultaConexionDmPlataforma(String numeroGuia) throws IOException;
	
}
