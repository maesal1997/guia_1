package co.com.tcc.maestro.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.IDatosConsumirActuApp;
import co.com.tcc.maestro.dto.DatosUser;
import co.com.tcc.maestro.dto.ObjMensajes;
import co.com.tcc.maestro.exception.ValidacionDominioException;
import co.com.tcc.maestro.service.IDatosConsumirActuAppService;

@Service
public class DatosConsumirActuAppServiceImpl implements IDatosConsumirActuAppService{

	@Autowired
	private IDatosConsumirActuApp datosConsumirActuApp;
	
	@Override
	public List<DatosUser> obtenerUsuario() {
		List<DatosUser> datosUsuarioApp = new ArrayList<DatosUser>();
		datosUsuarioApp = datosConsumirActuApp.obtenerUsuario();
	        if (datosUsuarioApp.isEmpty()) {
	        	ObjMensajes objMensajes = new ObjMensajes();
				objMensajes.setMensajeSistema("No se han encontrado datos del usuario logueado");
			    objMensajes.setMensajeUsuario("No se han encontrado datos del usuario logueado");		
				throw new ValidacionDominioException(objMensajes);
	        }
			return datosUsuarioApp;
	}
	
	@Override
	public List<DatosUser> obtenerCiudadUsuarioToken(String token) {
		List<DatosUser> datosUsuarioApp = new ArrayList<DatosUser>();
		datosUsuarioApp = datosConsumirActuApp.obtenerCiudadUsuarioToken(token);
	        if (datosUsuarioApp.isEmpty()) {
	        	ObjMensajes objMensajes = new ObjMensajes();
				objMensajes.setMensajeSistema("No se ha encontrado la ciudad para el token ingresado");
			    objMensajes.setMensajeUsuario("No se ha encontrado la ciudad para el token ingresado");		
				throw new ValidacionDominioException(objMensajes);
	        } 
	        if (datosUsuarioApp.get(0).getCiudad() == null) {
	        	ObjMensajes objMensajes = new ObjMensajes();
				objMensajes.setMensajeSistema("No se ha encontrado la ciudad para el token ingresado");
			    objMensajes.setMensajeUsuario("No se ha encontrado la ciudad para el token ingresado");		
				throw new ValidacionDominioException(objMensajes);
	        } 
			return datosUsuarioApp;
	}

} 
