package co.com.tcc.maestro.dao;

import java.util.List;

import co.com.tcc.maestro.dto.DatosTrazaUnidad;

public interface IUbicacionTccUnidadDao {

	public List<DatosTrazaUnidad> obtenerUbicacion(String codigosUnidades);
}
