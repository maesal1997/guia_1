package co.com.tcc.maestro.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.impl.ParametrosDao;

@Service
public class ParametroService {

	@Autowired
	private ParametrosDao parametrosDao;

	public String consultarParametro(String parametro) {

		return parametrosDao.consultarParametro(parametro);
	}

}
