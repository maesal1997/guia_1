package co.com.tcc.maestro.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.IActualizarCodigoUnidadDao;
import co.com.tcc.maestro.dto.Coordenada;
import co.com.tcc.maestro.dto.DatosTrazaUnidad;
import co.com.tcc.maestro.dto.DatosUser;
import co.com.tcc.maestro.dto.ObjMensajes;
import co.com.tcc.maestro.dto.ResultadoConsultaGeneral;
import co.com.tcc.maestro.exception.ValidacionDominioException;
import co.com.tcc.maestro.service.IActualizarCodigoUnidadService;
import co.com.tcc.maestro.service.ICoordenadasCiudadService;
import co.com.tcc.maestro.service.IDatosConsumirActuAppService;
import co.com.tcc.maestro.service.IUbicacionTccUnidadService;
import co.com.tcc.maestro.service.IValidarCodigoUnidadService;
import co.com.tcc.maestro.service.TokenService;

@Service
public class ActualizarCodigoUnidadServiceImpl implements IActualizarCodigoUnidadService {

	@Autowired
	IValidarCodigoUnidadService validarCodigoUnidadService;
	
	@Autowired
	IDatosConsumirActuAppService datosConsumirActuAppService;
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	ICoordenadasCiudadService coordenadasCiudadService;
	
	@Autowired
	IUbicacionTccUnidadService ubicacionTccUnidadService;
	
	@Autowired
	IActualizarCodigoUnidadDao actualizarCodigoUnidadDao;

	@Override
	public void actualizarCodigoUnidad(List<ResultadoConsultaGeneral> data, 
			                        String token,
									String usuario,
									String ciudad,
									String longitud,
									String latitud) {
	
		
		/*Se validad credenciales*/
		if ( (token == null || !token.matches(".*\\w.*")) && (usuario == null || !usuario.matches(".*\\w.*"))) {
	
			ObjMensajes objMensajes = new ObjMensajes();
			objMensajes.setMensajeSistema("Debe de ingresar un token o un usuario para poder realizar una actualizaci�n");
			objMensajes.setMensajeUsuario("Debe de ingresar un token o un usuario para poder realizar una actualizaci�n");
			throw new ValidacionDominioException(objMensajes);
			
		}
	
		List<DatosUser> ciudadesUsuarioToken = new ArrayList<DatosUser>();
		if (token != null && token.matches(".*\\w.*")) {
			tokenService.validarToken(token);
			ciudadesUsuarioToken = datosConsumirActuAppService.obtenerCiudadUsuarioToken(token);
		    ciudad = ciudadesUsuarioToken.get(0).getCiudad();
		    usuario =  ciudadesUsuarioToken.get(0).getUsuario();
		} else if (usuario != null && usuario.matches(".*\\w.*")) {
			if (ciudad == null || !ciudad.matches(".*\\w.*")) {
				ObjMensajes objMensajes = new ObjMensajes();
				objMensajes.setMensajeSistema("Debe de ingresar el id de la ciudad, cuando se consume el servicio por usuario");
				objMensajes.setMensajeUsuario("Debe de ingresar el id de la ciudad, cuando se consume el servicio por usuario");
				throw new ValidacionDominioException(objMensajes);
			}
			
		}
		/*Buscamos datos en caso de estar nulos o vacios para insertar trazabilidad*/
		
		List<Coordenada> coordenadas = new ArrayList<Coordenada>();
		if ((longitud == null || !longitud.matches(".*\\w.*")) && (latitud == null || !latitud.matches(".*\\w.*"))) {
			coordenadas = coordenadasCiudadService.obtenerCoodenadasCiudad(ciudad);
			longitud = coordenadas.get(0).getLongitud();
			latitud = coordenadas.get(0).getLatitud();
		}
		//Se deben ingresar ambas
		if ((longitud == null || !longitud.matches(".*\\w.*")) || (latitud == null || !latitud.matches(".*\\w.*"))) {
			ObjMensajes objMensajes = new ObjMensajes();
			objMensajes.setMensajeSistema("Debe suministrar logitud y latitud");
			objMensajes.setMensajeUsuario("Debe suministrar logitud y latitud");
			throw new ValidacionDominioException(objMensajes);
		}
		
		/*
		 * Se eliminan los resgistros con nuevos codigos null
		 */
		String nuevosCodigosUnidad = "";
		String antiguoCodigoUnidad = "";
		int contarCodigoUnidadNull = 0;
		ArrayList<ResultadoConsultaGeneral> datos = new ArrayList<ResultadoConsultaGeneral>();
		for (int i = 0; i < data.size(); i++) {
			if (data.get(i).getNuevoCodigo() != null && data.get(i).getNuevoCodigo().matches(".*\\w.*")) {
				nuevosCodigosUnidad += "," +"'"+data.get(i).getNuevoCodigo()+"'";
				antiguoCodigoUnidad += "," +"'"+data.get(i).getCodigoUnidad()+"'";
				datos.add(data.get(i));
			} else {
				contarCodigoUnidadNull = contarCodigoUnidadNull + 1;
			}
		}
		
		if (data.size() == contarCodigoUnidadNull ) {
			ObjMensajes objMensajes = new ObjMensajes();
			objMensajes.setMensajeSistema("No hay c�digos nuevos para actualizar");
			objMensajes.setMensajeUsuario("No hay c�digos nuevos para actualizar");
			throw new ValidacionDominioException(objMensajes);
		}

		/*
		 * se valida si vienen nuevos codigos repetidos
		 */
		String nuevoCodigosRepetidos = "";
		List<String> listaCodigosRepetidos = new ArrayList<String>();
		for (int i = 0; i < datos.size(); i++) {
			int contadorNuevoCodigosRepetidos = 0;
			for (int j = 0; j < datos.size(); j++) {
				if (datos.get(i).getNuevoCodigo().equals(datos.get(j).getNuevoCodigo())) {
					contadorNuevoCodigosRepetidos = contadorNuevoCodigosRepetidos + 1;
				}
			}

			if (contadorNuevoCodigosRepetidos > 1) {
				listaCodigosRepetidos.add(datos.get(i).getNuevoCodigo());
			}
		}
        /*Se eliminan duplicados codigos repetidos */
		Set<String> codigos = new HashSet<String>(listaCodigosRepetidos);
		listaCodigosRepetidos.clear();
		listaCodigosRepetidos.addAll(codigos);
		
		if (codigos.size() > 0) {
			
			for (String s : codigos) {
	            
				nuevoCodigosRepetidos += ", " + s;
	        }
			
			if (nuevoCodigosRepetidos != null) {
				ObjMensajes objMensajes = new ObjMensajes();
				String mensaje =  "El o los nuevo(s) c�digo(s): " + nuevoCodigosRepetidos.substring(1, nuevoCodigosRepetidos.length()) + " se encuentra(n) repetido(s)";
				objMensajes.setMensajeSistema( mensaje );
				objMensajes.setMensajeUsuario( mensaje );
				throw new ValidacionDominioException(objMensajes);
			}
		}
		

		/*
		 * Se valida que los nuevos codigos no existan en bd
		 */
		nuevosCodigosUnidad = nuevosCodigosUnidad.substring(1, nuevosCodigosUnidad.length());
		
		validarCodigoUnidadService.validarCodigoUnidad(nuevosCodigosUnidad);
		
		/*
		 * Se consigue la ubicaci�n de tcc de cada unidad 
		 */
		
		List<DatosTrazaUnidad> datosTrazaUnidad = new ArrayList<DatosTrazaUnidad>();
		antiguoCodigoUnidad = antiguoCodigoUnidad.substring(1, antiguoCodigoUnidad.length());
		datosTrazaUnidad = ubicacionTccUnidadService.obtenerUbicacion(antiguoCodigoUnidad);
		
		for (int i = 0; i < datosTrazaUnidad.size(); i++) {
			
			datos.get(i).setUbicacionTcc(datosTrazaUnidad.get(i).getUbicacionTcc());
			datos.get(i).setIdUnidad(datosTrazaUnidad.get(i).getIdUnidad());
		}
		
		/*
		 * Se actualiza
		 */
		actualizarCodigoUnidadDao.actualizarCodigoUnidad(datos, 
				                                         usuario,
				                                         ciudad,
				  			                             longitud,
				  			                             latitud);
	}

}
