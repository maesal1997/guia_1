package co.com.tcc.maestro.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.IUbicacionTccUnidadDao;
import co.com.tcc.maestro.dto.DatosTrazaUnidad;
import co.com.tcc.maestro.service.IUbicacionTccUnidadService;


@Service
public class UbicacionTccUnidadesServiceImpl implements IUbicacionTccUnidadService {

	@Autowired
	IUbicacionTccUnidadDao ubicacionTccUnidadDao;
	
	@Override
	public List<DatosTrazaUnidad> obtenerUbicacion(String codigosUnidades) {
		List<DatosTrazaUnidad> respuesta = new ArrayList<DatosTrazaUnidad>();
		respuesta = ubicacionTccUnidadDao.obtenerUbicacion(codigosUnidades);
		return respuesta;
	}

}
