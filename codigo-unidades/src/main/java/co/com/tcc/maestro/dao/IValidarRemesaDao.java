package co.com.tcc.maestro.dao;

import java.util.List;

public interface IValidarRemesaDao {

	public List<String> validarRemesa(String numeroGuia);
	
}
