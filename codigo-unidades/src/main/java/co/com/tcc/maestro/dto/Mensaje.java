package co.com.tcc.maestro.dto;

public class Mensaje {
    
	private int codigoSistema;
	private int codigoUsuario;
	private String mensaje;
	private Object body;
	private String stackTrace;

	/**
	 * @param mensaje
	 */

	public Mensaje() {

	}

	public Mensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Mensaje(String mensaje, String stackTrace, int codigoSistema, int codigoUsuario) {
		this.codigoSistema = codigoSistema;
		this.codigoUsuario = codigoUsuario;
		this.mensaje = mensaje;
		this.stackTrace = stackTrace;
	}

	public Mensaje(String mensaje, String strackTrace, Object body) {
		this.mensaje = mensaje;
		this.stackTrace = strackTrace;
		this.body = body;
	}

	/**
	 * @param mensaje
	 * @param body
	 */

	public Mensaje(String mensaje, Object body) {
		this.mensaje = mensaje;
		this.body = body;
	}

	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	/**
	 * @return the body
	 */
	public Object getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(Object body) {
		this.body = body;
	}

	public String getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}

	public int getCodigoSistema() {
		return codigoSistema;
	}

	public void setCodigoSistema(int codigoSistema) {
		this.codigoSistema = codigoSistema;
	}

	public int getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(int codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	
}