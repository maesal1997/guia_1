package co.com.tcc.maestro.exception;

import co.com.tcc.maestro.dto.ObjMensajes;

public class ValidacionDominioException extends RuntimeException {
    
	private ObjMensajes mensaje;
	/**
	 * 
	 */
	private static final long serialVersionUID = -6659405421519686252L;

	public ValidacionDominioException(ObjMensajes mensaje) {
		this.mensaje = mensaje;
	}

	public ObjMensajes getMensaje() {
		return mensaje;
	}

	public void setMensaje(ObjMensajes mensaje) {
		this.mensaje = mensaje;
	}

	
	
	
}