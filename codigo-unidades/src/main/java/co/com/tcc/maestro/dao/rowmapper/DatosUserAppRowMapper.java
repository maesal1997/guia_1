package co.com.tcc.maestro.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.com.tcc.maestro.dto.DatosUser;

public class DatosUserAppRowMapper implements RowMapper<DatosUser>{

	@Override
	public DatosUser mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		DatosUser datosUserApp = new DatosUser();
		datosUserApp.setCiudad(rs.getString("CIUD_ID_INT"));
		datosUserApp.setUsuario(rs.getString("USUA_LOGIN"));
		
		return datosUserApp;
	}


}
