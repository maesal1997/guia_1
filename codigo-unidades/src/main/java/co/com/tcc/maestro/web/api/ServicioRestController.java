package co.com.tcc.maestro.web.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.tcc.maestro.dto.DatosUser;
import co.com.tcc.maestro.dto.Respuesta;
import co.com.tcc.maestro.dto.ResultadoConsultaGeneral;
import co.com.tcc.maestro.service.IActualizarCodigoUnidadService;
import co.com.tcc.maestro.service.IConsultaGeneralService;
import co.com.tcc.maestro.service.IDatosConsumirActuAppService;
import co.com.tcc.maestro.service.IValidarRemesaService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ServicioRestController {

	private static final String API = "api/";

	
	@Autowired
	private IValidarRemesaService validarRemesaService;
	
	@Autowired 
	private IActualizarCodigoUnidadService actualizarCodigoUnidadService;
	
	@Autowired
	private IDatosConsumirActuAppService datosConsumirActuAppService;
	
	@Autowired
	private IConsultaGeneralService servicioService;

	@RequestMapping(value = API + "consulta/general", method = RequestMethod.POST)
	public Respuesta consultarGeneral(@RequestParam(required = true) String numeroGuia) throws IOException {
		validarRemesaService.validarRemesa(numeroGuia);
		return servicioService.consultaConexionDmPlataforma(numeroGuia);
	}
	

	@RequestMapping(value = API + "actualizar/codigo/unidad", method = RequestMethod.POST)
	public void actualizarCodigoUnidad(@RequestBody(required = true) List<ResultadoConsultaGeneral> data,
			@RequestHeader(required = false) String AccessToken,
			@RequestParam(required = false) String usuario,
			@RequestParam(required = false) String ciudad,
			@RequestParam(required = false) String longitud,
			@RequestParam(required = false) String latitud) throws IOException {
		
		actualizarCodigoUnidadService.actualizarCodigoUnidad(data, 
				                                             AccessToken,
				                                             usuario, 
				                                             ciudad,
				                                             longitud,
				                                             latitud
				                                             );
	}
	
	@RequestMapping(value = API + "consulta/usuario", method = RequestMethod.GET)
	public List<DatosUser> datosUsuario() {
		List<DatosUser> datosUsuarioApp = new ArrayList<DatosUser>();
		datosUsuarioApp = datosConsumirActuAppService.obtenerUsuario();
		return datosUsuarioApp;
	}
}
