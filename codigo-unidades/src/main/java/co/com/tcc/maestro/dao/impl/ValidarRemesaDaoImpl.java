package co.com.tcc.maestro.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dao.IValidarRemesaDao;

@Repository
public class ValidarRemesaDaoImpl implements IValidarRemesaDao {

	@Autowired
	@Qualifier("jdbcTemplateConsultaGeneral")
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<String> validarRemesa(String numeroGuia) {

		String sql = " SELECT REME_NUMERO_REMESA FROM REM_REMESAS WHERE REME_NUMERO_REMESA = ?";
		return jdbcTemplate.queryForList(sql, new Object[] { numeroGuia }, String.class);

	}

}
