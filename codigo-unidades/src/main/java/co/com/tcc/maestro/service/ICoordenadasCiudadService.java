package co.com.tcc.maestro.service;

import java.util.List;

import co.com.tcc.maestro.dto.Coordenada;

public interface ICoordenadasCiudadService {
	public List<Coordenada> obtenerCoodenadasCiudad(String ciudad);

}
