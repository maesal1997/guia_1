package co.com.tcc.maestro.dao;

import java.util.List;

public interface IValidarCodigoUnidadDao {

	public List<String> validarCodigoUnidad(String nuevoCodigoUnidad);
}
