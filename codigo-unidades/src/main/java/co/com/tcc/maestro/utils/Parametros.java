package co.com.tcc.maestro.utils;

/**
 * Clase que contiene el nombre de los par�metros de sistema
 * 
 * @author Joan Roa
 *
 */
public class Parametros {

     public static final String PREFIJO_USUARIO_APLICACION = "REC";
	
	/**
	 * Medios de distribuci�n que requieren documentaci�n.
	 */
	public static final String MEDIOS_DIST_REQ_DOC = "MEDIOS_DIST_REQ_DOC";
	/**
	 * Mae parametro sistema texto tratamiento de datos.
	 */
	public static final String AUTORIZA_TRATA_DATOS = "AUTORIZA_TRATA_DATOS";
	/*
	 * Datos para la trazabilidad
	 */
	public static final String ESON_REMESA_IMPO = "ESON_REMESA_IMPO";
	public static final String PROCESO_RECOLECCION = "PROCESO_RECOLECCION";
	


}
