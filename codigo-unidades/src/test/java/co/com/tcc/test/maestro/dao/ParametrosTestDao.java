package co.com.tcc.test.maestro.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.com.tcc.maestro.dao.impl.ParametrosDao;
import co.com.tcc.maestro.utils.JsonTransformer;
import co.com.tcc.test.maestro.config.AppConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class })
public class ParametrosTestDao {

	@Autowired
	private ParametrosDao parametrosDAO;

	@Test
	public void consultarParametros() {

		List<String> nombresParametros = new ArrayList<String>();
		nombresParametros.add("ROL_CONFIGURACION_CITA_MALLA");
		nombresParametros.add("ROL_CONSULTA_LOCALIZACIONES");
		nombresParametros.add("MEDIOS_DIST_REQ_DOC");

		Map<String, Object> parametros = parametrosDAO.consultarParametros(nombresParametros);

		System.out.println(JsonTransformer.toJson(parametros));
		assertTrue(parametros.size() > 0);

	}

}
