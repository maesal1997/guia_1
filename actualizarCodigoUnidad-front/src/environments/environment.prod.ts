export const environment = {
  production: true,
  apiBase : location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/codigo-unidades/'
};
