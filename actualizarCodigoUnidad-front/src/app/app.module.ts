import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgGridModule } from 'ag-grid-angular';
import { VistaPrincipalComponent } from './components/vista-principal/vista-principal.component';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';

import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material/core';

import { LecturaCodigoBarraComponent } from './components/lectura-qr/lectura-codigoBarra.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { AppUiModule } from './components/lectura-qr/app-ui.module';
import { AppInfoComponent } from './components/lectura-qr/app-info/app-info.component';
import { AppInfoDialogComponent } from './components/lectura-qr/app-info-dialog/app-info-dialog.component';
import { FormatsDialogComponent } from './components/lectura-qr/formats-dialog/formats-dialog.component';
import { BotonesGridComponent } from './utilComponents/boton-grid/botonGridServicios.component';
import { BlockUIModule } from 'ng-block-ui';
import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  declarations: [
    AppComponent,
    VistaPrincipalComponent,
    LecturaCodigoBarraComponent,
    AppInfoComponent,
    AppInfoDialogComponent,
    FormatsDialogComponent,
    BotonesGridComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    FormsModule,
    ZXingScannerModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatCheckboxModule,
  MatListModule,
  MatTooltipModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSelectModule,
  MatInputModule,
  MatMenuModule,
  MatIconModule,
  BlockUIModule.forRoot(),
    AgGridModule.withComponents(
      [
        BotonesGridComponent
      ]),
    AgGridModule,
     // Local
     AppUiModule,
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
    ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
