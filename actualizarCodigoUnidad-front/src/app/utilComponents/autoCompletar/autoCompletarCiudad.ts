import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { startWith } from 'rxjs/operators';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AutoCompletarCiudad {
    /* filtros autocompletar */
    options: any[];
    
  filter(descripcion: any): any[] {
   
    if (descripcion instanceof Object) {
      return;
    }
    const filterValue = descripcion.toLowerCase();

    return this.options.filter(option => option.descripcion.toLowerCase().indexOf(filterValue) === 0);
  }
  form(myControl: AbstractControl) {
    return myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => value),
        map(descripcion => {
          if (myControl.value === '') {
            myControl.patchValue(null);
          }
          return descripcion ? this.filter(descripcion) : this.options;
        })
      );
  }
}