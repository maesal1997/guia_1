import { Component, ɵConsole, Injectable, Input } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-boton-camara',
  template: `<span>
                   <mat-icon style="color: #449d44  !important; background-color: #fff !important; border-color: #398439 !important ;" aria-hidden="false" (click)="escanearCodigoBarra()" aria-label="Example home icon">camera_alt</mat-icon>
              </span>`,
  styleUrls: ['./boton-estilos.css']
})
export class BotonesGridComponent implements ICellRendererAngularComp {
  public datos: any[];
  public params: any;
  agInit(params: any): void {
    this.params = params;
    this.datos = [];
  }
  refresh(): boolean {
    return false;
  }
  // tslint:disable-next-line: typedef
  escanearCodigoBarra() {
    this.params.context.componentParent.escanearCodigoBarra(this.params.rowIndex);
  }
}
