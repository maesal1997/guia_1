import { Component, OnInit, Output, Inject } from '@angular/core';
import { BarcodeFormat } from '@zxing/library';
import { BehaviorSubject } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormatsDialogComponent } from './formats-dialog/formats-dialog.component';
import { AppInfoDialogComponent } from './app-info-dialog/app-info-dialog.component';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-lecturaCodigoBarra',
  templateUrl: './lectura-codigoBarra.component.html',
  styleUrls: ['./lectura-codigoBarra.component.css']
})
export class LecturaCodigoBarraComponent implements OnInit {

  availableDevices: MediaDeviceInfo[];
  currentDevice: MediaDeviceInfo = null;

  formatsEnabled: BarcodeFormat[] = [
    BarcodeFormat.CODE_128,
    BarcodeFormat.DATA_MATRIX,
    BarcodeFormat.EAN_13,
    BarcodeFormat.EAN_8,
    BarcodeFormat.ITF,
    BarcodeFormat.QR_CODE,
    BarcodeFormat.RSS_14,
  ];

  hasDevices: boolean;
  hasPermission: boolean;
  ocultarSerlecion: boolean;
  torchEnabled = true;
  torchAvailable$ = new BehaviorSubject<boolean>(false);
  tryHarder = false;
  ocultarForzar = true;
  @Output() qrResultString: string;
  constructor(private readonly dialog: MatDialog,
              public dialogRef: MatDialogRef<LecturaCodigoBarraComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
  }

  clearResult(): void {
    this.qrResultString = null;
  }

  onCamerasFound(devices: MediaDeviceInfo[]): void {
    this.availableDevices = devices;
    this.hasDevices = Boolean(devices && devices.length);
  }

  // tslint:disable-next-line: typedef
  onCodeResult(resultString: string) {
    this.qrResultString = resultString;
  }

  // tslint:disable-next-line: typedef
  onDeviceSelectChange(selected: string) {
    const device = this.availableDevices.find(x => x.deviceId === selected);
    this.currentDevice = device || null;
  }

  // tslint:disable-next-line: typedef
  openFormatsDialog() {
    const data = {
      formatsEnabled: this.formatsEnabled,
    };

    this.dialog
      .open(FormatsDialogComponent, { data })
      .afterClosed()
      .subscribe(x => { if (x) { this.formatsEnabled = x; } });
  }

  // tslint:disable-next-line: typedef
  onHasPermission(has: boolean) {
    this.hasPermission = has;
  }

  // tslint:disable-next-line: typedef
  openInfoDialog() {
    const data = {
      hasDevices: this.hasDevices,
      hasPermission: this.hasPermission,
    };

    this.dialog.open(AppInfoDialogComponent, { data });
  }

  onTorchCompatible(isCompatible: boolean): void {
    this.torchAvailable$.next(isCompatible || false);
  }
  toggleTorch(): void {
    this.torchEnabled = !this.torchEnabled;
  }

  toggleTryHarder(): void {
    this.tryHarder = !this.tryHarder;
    this.ocultarForzar = false;
  }

}
