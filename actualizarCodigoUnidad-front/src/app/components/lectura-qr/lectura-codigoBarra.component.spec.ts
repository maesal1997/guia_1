import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LecturaCodigoBarraComponent } from './lectura-codigoBarra.component';

describe('LecturaCodigoBarraComponent', () => {
  let component: LecturaCodigoBarraComponent;
  let fixture: ComponentFixture<LecturaCodigoBarraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LecturaCodigoBarraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LecturaCodigoBarraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
