import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelectionListChange } from '@angular/material/list';
import { BarcodeFormat } from '@zxing/library';
import { formatNames, formatsAvailable } from '../barcode-formats';

@Component({
  selector: 'app-formats-dialog',
  templateUrl: './formats-dialog.component.html',
  styleUrls: ['./formats-dialog.component.scss']
})
export class FormatsDialogComponent {

  formatsAvailable = formatsAvailable;

  formatsEnabled: BarcodeFormat[];

  readonly formatNames = formatNames;

  constructor(
    @Inject(MAT_DIALOG_DATA) readonly data: any,
    private readonly dialogRef: MatDialogRef<FormatsDialogComponent>,
  ) {
    this.formatsEnabled = data.formatsEnabled || [];
  }

  // tslint:disable-next-line: typedef
  close() {
    this.dialogRef.close(this.formatsEnabled);
  }

  // tslint:disable-next-line: typedef
  isEnabled(format: BarcodeFormat) {
    return this.formatsEnabled.find(x => x === format);
  }

  // tslint:disable-next-line: typedef
  onSelectionChange(event: MatSelectionListChange) {
    this.formatsEnabled = event.source.selectedOptions.selected.map(selected => selected.value);
  }
}
