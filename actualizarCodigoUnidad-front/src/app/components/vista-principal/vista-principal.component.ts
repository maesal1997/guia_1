import { Component, OnInit, Renderer2, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GridOptions } from 'ag-grid-community';
import { ColumnUnidades } from '../../utilComponents/Grid/ColumnUnidades';
import { ConsultaGeneralService } from '../../Services/ConsultaGeneralService';
import { AlertasConfirmacion } from '../../utilComponents/Alertas/AlertasConfirmacion';
import { ActualizarService } from '../../Services/ActualizarService';
import { BotonesGridComponent } from '../../utilComponents/boton-grid/botonGridServicios.component';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { LecturaCodigoBarraComponent } from '../lectura-qr/lectura-codigoBarra.component';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-vista-principal',
  templateUrl: './vista-principal.component.html',
  styles: [],
})
export class VistaPrincipalComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;
  /* titulo de la*/
  titulo = 'Actualización de códigos de unidades';
  /*Datos del formulario */
  busqueda: FormGroup;
  /* grid*/
  param;
  gridApi: any;
  gridColumnaApi: any;
  gridopciones: any;
  columnDefs: any;
  rowData: any[] = [];
  defaultColDef: any;
  domLayout: any;
  tamanoRegistros = 15;
  paginadorTamanoRegistro = [15, 30, 100, 500];
  mostrarGrid = true;
  getRowNodeId: any;
  /*input remesa*/
  eventoCampoNumeroRemesa: any;
  /*mensajes informativos*/
  mensaje: any;
  dialogRef: any;
  nuevoCodigo: any;
  /*Datos remitente y destinatario*/
  nombreRemite: any;
  cedulaRemite: any;
  ciudadRemite: any;
  direccionRemite: any;
  nombreDestinatario: any;
  cedulaDestinatario: any;
  ciudadDestinatario: any;
  direccionDestinatario: any;
  unnidadNegocio: any;
  estadoRemesa: any;
  numeroUnidades: any;
  /*Datos para el servicio actualizar*/
  usuario: any;
  ciudad: any;
  constructor(
    private formBuilder: FormBuilder,
    private columnUnidades: ColumnUnidades,
    private renderer: Renderer2,
    private consultaGeneralService: ConsultaGeneralService,
    private dialogosInformativos: AlertasConfirmacion,
    private actualizarService: ActualizarService,
    private router: Router,
    public dialog: MatDialog
  ) {
    /*DATOS DE LA GRID */
    this.columnDefs = this.columnUnidades.columnasGridUnidades();
    this.defaultColDef = { resizable: true };
    this.domLayout = 'autoHeight';
    this.getRowNodeId = function (data) {
      return data.idUnidad;
    };
    this.gridopciones = {
      context: {
        componentParent: this
      }
    } as GridOptions;
    this.gridopciones.rowData = this.rowData;
    this.gridopciones.columnDefs = this.columnDefs;
    this.frameworkComponents = {
      btnCamara: BotonesGridComponent
    };
  }
  frameworkComponents: { btnCamara: typeof BotonesGridComponent; };
  ngOnInit(): void {
    this.construirFormulario();
    this.focusNumeroRemesa();
  }
  /* froms group */
  // tslint:disable-next-line: typedef
  private construirFormulario() {
    this.busqueda = this.formBuilder.group({
      numeroRemesa: [null, [Validators.required]]
    });
  }
  /*Focus en el numero de guia*/
  // tslint:disable-next-line: typedef
  focusNumeroRemesa() {
    setTimeout(() => {
      this.renderer.selectRootElement('#numeroRemesa').focus();
    }, 1000);
  }
  /* grid */
  // tslint:disable-next-line: typedef
  onGridReady(params) {
    this.gridApi = params.api;
    this.param = params;
    this.gridColumnaApi = params.columnaApi;
    this.gridApi.hideOverlay();
    params.api.setRowData(this.rowData);
  }
  // tslint:disable-next-line: typedef
  onPageSizeChanged(size: number) {
    this.gridApi.paginationSetPageSize(Number(size));
  }
  /*eventos de cambio del numero de guia*/
  onSearchChangeNumeroRemesa(searchValue: string): void {
    this.eventoCampoNumeroRemesa = searchValue;
    if (this.eventoCampoNumeroRemesa.length < 1) {
      this.focusNumeroRemesa();
      this.busqueda.reset();
      this.limpiar();
    }
  }
  // tslint:disable-next-line: typedef
  limpiar() {
    this.nombreRemite = '';
    this.cedulaRemite = '';
    this.ciudadRemite = '';
    this.direccionRemite = '';
    this.nombreDestinatario = '';
    this.cedulaDestinatario = '';
    this.ciudadDestinatario = '';
    this.direccionDestinatario = '';
    this.unnidadNegocio = '';
    this.estadoRemesa = '';
    this.numeroUnidades = '';
    this.rowData = [];
  }
  // tslint:disable-next-line: typedef
  nuevaColsulta() {
    this.focusNumeroRemesa();
    this.busqueda.reset();
    this.limpiar();
  }

  /* obtener datos del formulario */
  // tslint:disable-next-line: typedef
  consultar() {
    this.limpiar();
    if (this.busqueda.status !== 'INVALID') {
      this.blockUI.start('Consultando, por favor espere...');
      this.consultaGeneralService.consultaGeneralPorFiltros(this.busqueda.controls.numeroRemesa.value).subscribe((data: any) => {
        this.blockUI.stop();
        this.nombreRemite = data.data[0].nombreRemite;
        this.cedulaRemite = data.data[0].nitRemite;
        this.ciudadRemite = data.data[0].ciudadRemite;
        this.direccionRemite = data.data[0].direccionRemite;
        this.nombreDestinatario = data.data[0].nombreDestinatario;
        this.cedulaDestinatario = data.data[0].nitDestinatario;
        this.ciudadDestinatario = data.data[0].ciudadDestinatario;
        this.direccionDestinatario = data.data[0].direccionDestinatario;
        this.unnidadNegocio = data.data[0].unidadNegocio;
        this.estadoRemesa = data.data[0].estadoOperativo;
        this.rowData = data.data;
        this.numeroUnidades = this.rowData.length;
        this.conseguirUsuario();
      }, err => {
        this.blockUI.stop();
        if (err.error.mensajeUsuario === err.error.mensajeSistema) {
          this.mensaje = err.error.mensajeSistema;
        } else {
          this.mensaje = err.error.mensajeUsuario + ' ' + err.error.mensajeSistema;
        }
        if (this.mensaje !== '' || this.mensaje !== null) {
          this.dialogosInformativos.informacion('Información: ', this.mensaje, 'error');
        } else {
          this.dialogosInformativos.informacion('Información: ', 'Error al tratar de consumir el servicio', 'error');
        }
      });
    } else {
      this.dialogosInformativos.informacion('Información: ', 'No ha digitado un número de guía, por favor digite el número', 'info');
    }
  }
  /* obtener datos de la grid y los actualiza */
  // tslint:disable-next-line: typedef
  async actualizar() {

    if (this.rowData.length > 0) {
      // tslint:disable-next-line: max-line-length
      const respuesta = await this.dialogosInformativos.mensaje('Información: ', '¿Está seguro de que desea actualizar la información?', 'warning') as boolean
      if (respuesta === true) {
        this.blockUI.start('Consultando, por favor espere...');
        this.actualizarService.actualizarCodigoUnidad(this.rowData, this.usuario, this.ciudad).subscribe((data: any) => {
          this.consultar();
          this.blockUI.stop();
          this.dialogosInformativos.informacion('Información: ', 'Se han actualizando los códigos correctamente', 'success');
        }, err => {
          if (err.error.mensajeUsuario === err.error.mensajeSistema) {
            this.mensaje = err.error.mensajeSistema;
          } else {
            this.mensaje = err.error.mensajeUsuario + ' ' + err.error.mensajeSistema;
          }
          this.blockUI.stop();
          this.dialogosInformativos.informacion('Información: ', this.mensaje, 'error');
        });
      }
    } else {
      this.dialogosInformativos.informacion('Información: ', 'No hay datos para actualizar', 'info');
    }
  }

  /*conseguir usuario para consumir servicio */
  conseguirUsuario() {
    this.actualizarService.consultarDatosUsuario().subscribe((data: any) => {
      this.usuario = data[0].usuario;
      this.ciudad  = data[0].ciudad;
    }, err => {
      if (err.error.mensajeUsuario === err.error.mensajeSistema) {
        this.mensaje = err.error.mensajeSistema;
      } else {
        this.mensaje = err.error.mensajeUsuario + ' ' + err.error.mensajeSistema;
      }
      this.dialogosInformativos.informacion('Información: ', this.mensaje, 'error');
      return;
    }
    );
  }
  /*Escanear códigos de barra*/
  // tslint:disable-next-line: typedef
  escanearCodigoBarra(index: number) {
    this.dialogRef = this.dialog.open(LecturaCodigoBarraComponent,
      {
        data: this.nuevoCodigo,
        maxHeight: '90vh'
      });

    this.dialogRef.afterClosed().subscribe(result => {
      this.nuevoCodigo = result;
      const id = this.rowData[index].idUnidad;
      const rowNode = this.gridApi.getRowNode(id);
      rowNode.setDataValue('nuevoCodigo', this.nuevoCodigo);
    });
  }
}
